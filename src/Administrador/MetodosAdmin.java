package Administrador;

import java.util.ArrayList;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;


public class MetodosAdmin {

	private ObjectContainer db=null;
	 
	 private void abrirRegistro() {
		 
		 //Archivo donde se almacenan datos a la base de datos.
		 //Metodo para abrir la conexi�n
		 
	 db=Db4oEmbedded.openFile("registroUsuario");
			 }
	 
	 private void cerrarRegistro() {
	//Metodo para cerrar la conexi�n a la base de datos
		 db.close();
		 
	 }
	 
	 

	 

	public void insertarRegistro(User usuario) {

        abrirRegistro();
		//db.store  Este sintaxis inserta usuarios a la base de datos.
        db.store(usuario);
		cerrarRegistro();
		
	}

	public List<User> seleccionarUsuarios(){
		abrirRegistro();
		ObjectSet listaUsuarios=db.queryByExample(User.class);
		List<User> lp =new ArrayList<>();
		for (Object listaUsuarios1:listaUsuarios) {
			lp.add((User)listaUsuarios1);
			
		}
		cerrarRegistro();
		return lp;
		
	}

	//Metodo de Consulta para usuarios o datos de el.
	
	public User seleccionarUsuario(User usuarios) {
		abrirRegistro();
		ObjectSet resultado=db.queryByExample(usuarios);
		User usuario=(User) resultado.next();
		cerrarRegistro();
		return usuario;
		
	}
	//Metodo Update(Actualizar)

	public void actualizarRegistro(int id,String nombre,Double fondoInicial,int numTarjeta, int nip) {

		abrirRegistro();
		User p = new User();
		p.setNumTarjeta(numTarjeta);
		ObjectSet resultado=db.queryByExample(p);
		User auxiliar=(User) resultado.next();
		auxiliar.setIdUsuario(id);
		auxiliar.setNombreUsuario(nombre);
		auxiliar.setFondoInicial(fondoInicial);
		auxiliar.setNumTarjeta(numTarjeta);
		auxiliar.setNip(nip);
		db.store(auxiliar);
		cerrarRegistro();
	}

	public User eliminarRegistros(int id) {
		abrirRegistro();
		User usuario = new User();
		usuario.setIdUsuario(id);
		ObjectSet resultado=db.queryByExample(usuario);
		User auxiliar=(User) resultado.next();
		//Metodo delete(para eliminar) db.delete
		db.delete(auxiliar);
		cerrarRegistro();
		return auxiliar;
		
		
	}

	
	
}
