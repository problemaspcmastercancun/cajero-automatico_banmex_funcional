package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;

public class EliminarUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EliminarUsuario frame = new EliminarUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EliminarUsuario() {
		setTitle("BanMex");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("IDUsuario");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBounds(38, 101, 84, 14);
		contentPane.add(lblNewLabel);
		
		txtID = new JTextField();
		txtID.setBounds(165, 98, 86, 20);
		contentPane.add(txtID);
		txtID.setColumns(10);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				
				MetodosAdmin metodos=new  MetodosAdmin();
				User usuario=new User();
				int id;

				id=(Integer.parseInt(txtID.getText()));
				usuario = metodos.eliminarRegistros(id);
				
				JOptionPane.showMessageDialog(null, "Los datos seleccionados eliminados son: \n "+ usuario, getTitle(), JOptionPane.WARNING_MESSAGE);
				
				
			
				
			}
		});
		btnEliminar.setBounds(162, 157, 89, 23);
		contentPane.add(btnEliminar);
		
		JButton btnregresar = new JButton("Regresar");
		btnregresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				MenuPrincipal principal = new MenuPrincipal();
				principal.setVisible(true);
			
			}
		});
		btnregresar.setBounds(10, 227, 89, 23);
		contentPane.add(btnregresar);
		
		JLabel lblNewLabel_1 = new JLabel("Eliminar Usuarios.");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(68, 11, 281, 38);
		contentPane.add(lblNewLabel_1);
	}

}
