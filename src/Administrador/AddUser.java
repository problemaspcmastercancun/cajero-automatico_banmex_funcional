package Administrador;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import EDU.purdue.cs.bloat.decorate.Main;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;
import java.awt.TextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.SwingConstants;

public class AddUser extends JFrame {
//Declaramos
	private JPanel contentPane;
	private JTextField txtID;
	private JTextField txtNom;
	private JTextField txtTarjeta;
	private JTextField txtNip;
	private JTextField txtSaldo;
	TextArea textArea = 	new TextArea();
	
	Main menuu = new Main ();

	User user = new User();
	MetodosAdmin metodos = new MetodosAdmin();
	User usuarios = new User();
	

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					AddUser frame = new AddUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the frame.
	 */
	
	public AddUser() {
		setTitle("BanMex");
//validaci�n de edad, para saber si el usuario es mayor o requiere de un tutor para ingresar.
		
		int resp = JOptionPane.showConfirmDialog(null, "�Eres mayor de edad?");

		if (JOptionPane.YES_NO_OPTION == resp) {

		} else {
			JOptionPane.showInputDialog("Agregue el nombre de un mayor o de un tutor:");
		}
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 395, 453);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Nombre:");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBounds(21, 102, 86, 14);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("NIP: ");
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setBounds(21, 133, 46, 14);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("Saldo Actual:");
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setBounds(21, 164, 98, 14);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("ID:");
		lblNewLabel_4.setForeground(new Color(255, 255, 255));
		lblNewLabel_4.setBounds(21, 71, 46, 14);
		contentPane.add(lblNewLabel_4);

		txtID = new JTextField();
		txtID.setBounds(129, 68, 114, 20);
		contentPane.add(txtID);
		txtID.setColumns(10);

		txtNom = new JTextField();
		txtNom.setBounds(129, 99, 114, 20);
		contentPane.add(txtNom);
		txtNom.setColumns(10);

		txtTarjeta = new JTextField();
		txtTarjeta.setBounds(129, 197, 114, 20);
		contentPane.add(txtTarjeta);
		txtTarjeta.getColumns();
		txtTarjeta.setEnabled(false);
		
		

		JLabel lblNewLabel_1 = new JLabel("N\u00FAmero de Tarjeta:");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(10, 200, 152, 14);
		contentPane.add(lblNewLabel_1);
		

		txtNip = new JTextField();
		txtNip.setBounds(129, 130, 114, 20);
		contentPane.add(txtNip);
		txtNip.setColumns(10);

		txtSaldo = new JTextField();
		txtSaldo.setBounds(129, 161, 114, 20);
		contentPane.add(txtSaldo);
		txtSaldo.setColumns(10);

		JButton btnGuardar = new JButton("Agregar");
		// usuarios.verificarEdad();

		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				usuarios.setIdUsuario(Integer.parseInt(txtID.getText()));
				usuarios.setNombreUsuario(txtNom.getText());
				usuarios.getNumTarjeta();
				usuarios.setNip(Integer.parseInt(txtNip.getText()));
				usuarios.setFondoInicial(Double.parseDouble(txtSaldo.getText()));
				metodos.insertarRegistro(usuarios);

				txtID.setText("");
				txtNom.setText("");
				txtTarjeta.setText("");
				txtNip.setText("");
				txtSaldo.setText("");

				JOptionPane.showMessageDialog(null, "Datos Guardados: \n " + usuarios, getTitle(),
						JOptionPane.WARNING_MESSAGE);
				
				textArea.append(usuarios.toString());
				
				
			}

		});
		btnGuardar.setBounds(280, 117, 89, 23);
		contentPane.add(btnGuardar);

		JLabel lblNewLabel_5 = new JLabel("Agregar usuario.");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_5.setForeground(new Color(255, 255, 255));
		lblNewLabel_5.setFont(new Font("Arial", Font.PLAIN, 17));
		lblNewLabel_5.setBounds(106, 11, 170, 41);
		contentPane.add(lblNewLabel_5);

		JButton btnRegresar = new JButton("Regresar");
		btnRegresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRegresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				MenuPrincipal principal = new MenuPrincipal();
				principal.setVisible(true);

			}
		});
		btnRegresar.setBounds(10, 380, 89, 23);
		contentPane.add(btnRegresar);
		textArea.setEditable(false);
		
		textArea.setBounds(10, 265, 177, 105);
		contentPane.add(textArea);
		
		JLabel lblNewLabel_6 = new JLabel("Consola:");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_6.setForeground(new Color(255, 255, 255));
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_6.setBounds(10, 243, 114, 20);
		contentPane.add(lblNewLabel_6);
		


	}

	
	
}