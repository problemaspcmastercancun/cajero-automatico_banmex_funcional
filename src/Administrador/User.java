package Administrador;

import java.util.Random;

public class User {
	
	//Declaraciones de variables
	
 	private int idUsuario;
    private String nombreUsuario;
    private int nip;
    private int numTarjeta=55790830;
    private Double SaldoInicial;
	
	

//getters y setters
    public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public int getNip() {
		return nip;
	}
	public void setNip(int nip) {
		this.nip = nip;
	}
	public int getNumTarjeta() {
		return numTarjeta;
	}
	public void setNumTarjeta(int numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	public Double getFondoInicial() {
		return SaldoInicial;
	}
	public void setFondoInicial(Double fondoInicial) {
		this.SaldoInicial = fondoInicial;
	}
	
	 @Override
	    public String toString() {
	        return "Usuario"
	        		+ "\n" + "ID: " + idUsuario + "\n Nombre: " + nombreUsuario+ "\n"
	        			+ "Nu. Tarjeta: " + numTarjeta +"\n NIP: " + nip + "\n Saldo: $" + SaldoInicial ;
	    }
	 }
