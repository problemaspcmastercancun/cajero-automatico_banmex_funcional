package Cajero;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Administrador.MetodosAdmin;
import Administrador.MenuPrincipal;
import Administrador.User;
import Main.Main;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.SwingConstants;

public class PrincipalCajero extends JFrame {

	private JPanel contentPane;
	private JTextField txtNumTarjeta1;
	private JTextField txtNip1;
	JLabel lblNewLabel,lblNewLabel_1;
	JButton btnEntrar;
	private JLabel lbNombre;
	private JLabel lbFondos;
	private static JButton btnDepositar;
	private static JButton btnRetirar;
	private static JButton btnSalir;
	private static JLabel lblNewLabel_3;
	private static JTextField txtEfectivo;
	private JButton btnregresar;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalCajero frame = new PrincipalCajero();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrincipalCajero() {
		setTitle("BanMex");
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	    lblNewLabel = new JLabel("N\u00FAmero de tarjeta:");
	    lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setBounds(10, 101, 113, 14);
		contentPane.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("NIP:");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(10, 129, 73, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Bienvenido A Cajeros BanMex");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_2.setBounds(83, 11, 274, 55);
		contentPane.add(lblNewLabel_2);
		
		txtNumTarjeta1 = new JTextField();
		txtNumTarjeta1.addMouseListener(new MouseAdapter() {
			
		});
		txtNumTarjeta1.setBounds(134, 98, 129, 20);
		contentPane.add(txtNumTarjeta1);
		txtNumTarjeta1.setColumns(10);
		
		txtNip1 = new JTextField();
		txtNip1.setBounds(133, 126, 86, 20);
		contentPane.add(txtNip1);
		txtNip1.setColumns(10);
		
		 btnEntrar = new JButton("Entrar");
		 btnEntrar.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 	}
		 });
		btnEntrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				 lbNombre.setVisible(false);
				 lbFondos.setVisible(false);

			  MetodosAdmin metodos = new  MetodosAdmin();
			  User usuario=new User();
				
				usuario.setNumTarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				usuario = metodos.seleccionarUsuario(usuario);
				int tarjeta=(Integer.parseInt(txtNumTarjeta1.getText()));
				int tarjeta1=usuario.getNumTarjeta();
				int pin=(Integer.parseInt(txtNip1.getText()));
				int pin1=usuario.getNip();
				String  fondos=usuario.getFondoInicial().toString();
				if(tarjeta == tarjeta1 && pin == pin1 ){
					JOptionPane.showMessageDialog(null, "Bienvido A Bancos De M�xico: "+ usuario.getNombreUsuario(), " ", JOptionPane.INFORMATION_MESSAGE);
					lbNombre.setText(usuario.getNombreUsuario());
					lbFondos.setText("$"+fondos);
					Menuinicio();
				}else {
					
					JOptionPane.showMessageDialog(null, "NIP de la tarjeta: "+ tarjeta1 + " \n Valor Inv�lido", getTitle(), JOptionPane.WARNING_MESSAGE);
					
					
				}
					
			
			}
			
			
		});
		btnEntrar.setBounds(158, 168, 89, 23);
		contentPane.add(btnEntrar);
		
		lbNombre = new JLabel("New label");
		lbNombre.setForeground(new Color(255, 255, 255));
		lbNombre.setBounds(135, 101, 46, 14);
		contentPane.add(lbNombre);
		
		lbFondos = new JLabel("New label");
		lbFondos.setForeground(new Color(255, 255, 255));
		lbFondos.setBounds(133, 129, 46, 14);
		contentPane.add(lbFondos);
		
		btnDepositar = new JButton("Depositar");
		btnDepositar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MetodosAdmin metodos = new  MetodosAdmin();
				User usuarios=new User();
				
				usuarios.setNumTarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				usuarios= metodos.seleccionarUsuario(usuarios);
				
				
				
				int id= (usuarios.getIdUsuario());
				String nom=(usuarios.getNombreUsuario());
				Double fondF=Double.parseDouble(txtEfectivo.getText());
				Double fond1=(usuarios.getFondoInicial());
				int numTarjeta=(usuarios.getNumTarjeta());
				int NIP=(usuarios.getNip());
				Double fondI=fondF+fond1;
				
				metodos.actualizarRegistro(id, nom, fondI, numTarjeta, NIP);
				
				JOptionPane.showMessageDialog(null, "Cantidad depositada es: "+ fondF, getTitle(), JOptionPane.WARNING_MESSAGE);
				String  fondos=usuarios.getFondoInicial().toString();
				lbFondos.setText(fondos);
				
				
			}
		});
		btnDepositar.setVisible(false);
		btnDepositar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDepositar.setBounds(335, 97, 89, 23);
		contentPane.add(btnDepositar);
		
		btnRetirar = new JButton("Retirar");
		btnRetirar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MetodosAdmin metodos = new  MetodosAdmin();
				User usuarios=new User();
				usuarios.setNumTarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				usuarios= metodos.seleccionarUsuario(usuarios);
				
				
				
				int id= (usuarios.getIdUsuario());
				String nombre=(usuarios.getNombreUsuario());
				Double fondosIntroducidos=Double.parseDouble(txtEfectivo.getText());
				Double fondoInicial=(usuarios.getFondoInicial());
				int numTarjeta=(usuarios.getNumTarjeta());
				int NIP=(usuarios.getNip());
				
				if(fondosIntroducidos<=fondoInicial) {
					Double fondTotal=fondoInicial-fondosIntroducidos;
					
					metodos.actualizarRegistro(id, nombre, fondTotal, numTarjeta, NIP);

					JOptionPane.showMessageDialog(null, "Cantidad Retirada: "+ fondosIntroducidos, getTitle(), JOptionPane.WARNING_MESSAGE);
			

					String  fondos=usuarios.getFondoInicial().toString();
					lbFondos.setText(fondos);
					
				}else {
				
				JOptionPane.showMessageDialog(null, "No tiene fondos suficientes.", "Fondos Insuficientes", JOptionPane.WARNING_MESSAGE);

				String  fondos=usuarios.getFondoInicial().toString();
				lbFondos.setText(fondos);
				}
				
			 		
			 		
			 		

				
			}
		});
		btnRetirar.setVisible(false);
		btnRetirar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnRetirar.setBounds(335, 146, 89, 23);
		contentPane.add(btnRetirar);
		
		btnSalir = new JButton("Salir");
		btnSalir.setVisible(false);
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				System.exit(0);
				
				
			}
		});
		btnSalir.setBounds(335, 227, 89, 23);
		contentPane.add(btnSalir);
		
		lblNewLabel_3 = new JLabel("Cantidad:");
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setVisible(false);
		lblNewLabel_3.setBounds(32, 172, 129, 14);
		contentPane.add(lblNewLabel_3);
		
		txtEfectivo = new JTextField();
		txtEfectivo.setVisible(false);
		
		txtEfectivo.setBounds(161, 169, 86, 20);
		contentPane.add(txtEfectivo);
		txtEfectivo.setColumns(10);
		
		btnregresar = new JButton("Regresar");
		btnregresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Main principal = new Main();
				principal.setVisible(true);
				
			}
		});
		btnregresar.setBounds(10, 227, 89, 23);
		contentPane.add(btnregresar);
	}
	
	public void Menuinicio() {
		txtNumTarjeta1.setVisible(false);
		txtNip1.setVisible(false);
		btnEntrar.setVisible(false);
		btnregresar.setVisible(false);
		
		btnSalir.setVisible(true);
		btnDepositar.setVisible(true);
		btnRetirar.setVisible(true);
		
		lblNewLabel_3.setVisible(true); 
		txtEfectivo.setVisible(true);
		
		lblNewLabel.setText("Nombre: ");
		lblNewLabel_1.setText("Saldo: ");
		lbNombre.setVisible(true);
		lbFondos.setVisible(true);
	 
		
	}
	
}
